<html lang="ja">

<head>
  <meta charset="UTF-8">
  <title>sampleSachikoHTML</title>
  <link rel="stylesheet" type="text/css" href="./css/text.css">
  <script type="text/javascript" src="./js/simple.js"></script>
</head>

<body>

  <div class="wrapper">
    <header class="header">
    </header>
    <main role="main" class="main">
      <p><span class="bold">main</span></p>
    </main>
    <nav>
      <ul class="list-nav00">
        <li><a href="./php/info.php">設定ファイル</a></li>
        <li><a href="./php/hello.php">Hello</a></li>
        <li><a href="./php/sample.php">sample</a></li>
      </ul>
    </nav>
    <br>
    <img src="./jpg/shirakoma.jpg" width="300" height="300" alt="森">
    <footer class="footer">
      <p>footer</p>
    </footer>
    <p><button onclick="confirmFunction1()">移動しますか</button></p>
    <button onclick="clickEvent()">アラートの表示を行いますか。</button>
  </div>
</body>

</html>