<html lang="ja">

<head>
  <meta charset="UTF-8">
  <title>hello</title>
  <link rel="stylesheet" type="text/css" href="./css/text.css">
  <script type="text/javascript" src="./js/simple.js"></script>
</head>

<body>

  <p><span class="bold">HELLO</span></p>

  <?php
      echo "こんにちは";
      echo date('Y年m月d日 H時i分s秒');
  ?>
  
</body>

</html>